package com.example.picsum

import com.example.picsum.api.PictureData

interface MainContract {

    interface View {
        fun showPictures(pictures: List<PictureData>)
        fun showProgress()
        fun hideProgress()
        fun clearPictures()
    }

    interface Presenter {
        fun onCreate()
        fun onSwipeToRefresh()
        fun onScrolled(lastItem: Int, itemCount: Int)
    }
}