package com.example.picsum.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.picsum.R
import com.example.picsum.api.PictureData

class PicturesAdapter : RecyclerView.Adapter<PicturesAdapter.ViewHolder>() {

    private val pictures = mutableListOf<PictureData>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_picture, parent, false)
        )
    }

    override fun getItemCount(): Int = pictures.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(pictures[position])
    }

    fun addPictures(pictures: List<PictureData>) {
        this.pictures.addAll(pictures)
        notifyItemRangeInserted(this.pictures.size - pictures.size, pictures.size)
    }

    fun clearItems() {
        pictures.clear()
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val picture: ImageView = itemView.findViewById(R.id.picture)

        fun bind(pictureData: PictureData) {
            Glide.with(itemView)
                .load(pictureData.downloadUrl)
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .centerCrop()
                .placeholder(createCircularProgress(itemView.context))
                .into(picture)
        }

        private fun createCircularProgress(context: Context): CircularProgressDrawable {
            return CircularProgressDrawable(context).apply {
                strokeWidth = 5f
                centerRadius = 30f
                start()
            }
        }
    }
}