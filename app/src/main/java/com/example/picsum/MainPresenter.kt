package com.example.picsum

import android.util.Log
import com.example.picsum.api.PictureData
import com.example.picsum.api.PicturesRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

private const val PAGE_LIMIT = 30

class MainPresenter(
    private val view: MainContract.View,
    private val picturesRepository: PicturesRepository
) : MainContract.Presenter {

    private var page = 1
    private var isLoading = false

    override fun onCreate() {
        getPictures(false)
        view.showProgress()
    }

    override fun onSwipeToRefresh() {
        view.clearPictures()
        getPictures(true)
    }

    override fun onScrolled(lastItem: Int, itemCount: Int) {
        if (lastItem == itemCount - (PAGE_LIMIT / 2)) {
            getPictures(false)
        }
    }

    private fun getPictures(swipeRefresh: Boolean) {
        if (swipeRefresh) page = 1
        if (!isLoading) {
            isLoading = true
            picturesRepository.getPictures(page, PAGE_LIMIT)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ updatePictures(it) }, { logError(it)
                view.hideProgress()})
        }
    }

    private fun updatePictures(pictures: List<PictureData>) {
        page++
        isLoading = false
        view.hideProgress()
        view.showPictures(pictures)
    }

    private fun logError(throwable: Throwable) {
        Log.e("error", throwable.message ?: "some error")
    }
}