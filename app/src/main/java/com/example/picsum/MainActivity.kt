package com.example.picsum

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.picsum.adapter.PicturesAdapter
import com.example.picsum.api.PictureData
import com.example.picsum.api.PicturesRepositoryImpl
import com.example.picsum.api.ServiceFactory
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), MainContract.View {

    private val presenter: MainContract.Presenter = MainPresenter(this, PicturesRepositoryImpl(ServiceFactory.create()))
    private val picturesAdapter: PicturesAdapter = PicturesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViews()
        presenter.onCreate()
    }

    override fun showPictures(pictures: List<PictureData>) {
        picturesAdapter.addPictures(pictures)
    }

    override fun clearPictures() {
        picturesAdapter.clearItems()
    }

    private fun setupViews() {
        picsRecyclerView.adapter = picturesAdapter
        addListenerToRecyclerView()
        swipeRefreshLayout.setOnRefreshListener { presenter.onSwipeToRefresh() }
    }

    private fun addListenerToRecyclerView() {
        val layoutManager = picsRecyclerView.layoutManager as LinearLayoutManager
        picsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                presenter.onScrolled(
                    layoutManager.findLastVisibleItemPosition(),
                    layoutManager.itemCount
                )
            }
        })
    }

    override fun showProgress() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideProgress() {
        swipeRefreshLayout.isRefreshing = false
    }
}

