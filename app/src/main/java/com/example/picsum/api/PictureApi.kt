package com.example.picsum.api

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface PictureApi {

    @GET("v2/list?")
    fun getPictures(
        @Query("page")
        page: Int,
        @Query("limit")
        limit: Int
    ): Observable<MutableList<PictureData>>

}