package com.example.picsum.api

class PicturesRepositoryImpl(
    private val pictureApi: PictureApi
) : PicturesRepository {

    override fun getPictures(page: Int, limit: Int) = pictureApi.getPictures(page, limit)
}