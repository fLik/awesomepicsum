package com.example.picsum.api

import io.reactivex.Observable

interface PicturesRepository {
    fun getPictures(page: Int, limit: Int): Observable<MutableList<PictureData>>
}